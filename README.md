## Pranas (Frank) Ziaukas - résumé

This is the showcase of my personal experience, education, skills and achievements.

#### Download

The most up-to-date version of my CV can be downloaded [here](https://gitlab.com/pziaukas/CurriculumVitae/-/jobs/artifacts/master/raw/build/Ziaukas_CV.pdf?job=CV%20Builder).

#### Credits

[Awesome-CV](https://github.com/posquit0/Awesome-CV) design template is used.